require 'simplecov'
SimpleCov.start
require_relative '../lib/map.rb'

describe Map do

  context "#empty?" do
    it "return true if map is empty" do
      test_map = Map.new
      expect(test_map.empty?).to eq(true)
    end
    it "return false if map isn't empty" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      expect(test_map.empty?).to eq(false)
    end
  end

  context "#size" do
    it "return correct size" do
      test_map = Map.new
      test_items = [Item.new("a",1), Item.new("b",2), Item.new("c",3), Item.new("d",4), Item.new("e",5)]
      test_items.each { |x| test_map.insert(x) }
      expect(test_map.size).to eq(test_items.size)
    end
  end

  context "#toString" do
    it "return '(a, 1), (b, 2), (c, 3), (d, 4), (e, 5), '" do
      test_map = Map.new
      test_items = [Item.new("a",1), Item.new("b",2), Item.new("c",3), Item.new("d",4), Item.new("e",5)]
      test_items.each { |x| test_map.insert(x) }
      # puts(test_map.toString)
      expect(test_map.toString).to eq('(a, 1), (b, 2), (c, 3), (d, 4), (e, 5), ')
    end
  end

  context "#depth" do
    it "return correct depth" do
      test_map = Map.new
      test_items = [Item.new("a",1), Item.new("b",2), Item.new("c",3), Item.new("d",4), Item.new("e",5)]
      test_items.each { |x| test_map.insert(x) }
      result = test_map.depth == 5
      test_map = Map.new
      test_items = [Item.new("c",3), Item.new("d",4), Item.new("e",5), Item.new("a",1), Item.new("b",2)]
      test_items.each { |x| test_map.insert(x) }
      result &= test_map.depth == 3
      expect(result).to eq(true)
    end
  end

  context "#truncate" do
    it "empty list" do
      test_map = Map.new
      test_items = [Item.new("a",1), Item.new("b",2), Item.new("c",3), Item.new("d",4), Item.new("e",5)]
      test_items.each { |x| test_map.insert(x) }
      test_map.truncate
      expect(test_map.empty?).to eq(true)
    end
  end

  context "When adding element it should" do
    it "return true when map is empty" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      found_item = test_map.find("a")
      expect(found_item == test_item.value).to eq(true)
    end

    it "return false if item with specified key already exists in map" do
      test_map = Map.new
      test_item1 = Item.new("a", 3)
      test_item2 = Item.new("a", 5)
      test_map.insert(test_item1)
      insert_return = test_map.insert(test_item2)
      expect(insert_return).to eq(false)
    end

    it "return true if provided with unique key" do
      test_map = Map.new
      test_item1 = Item.new("a", 3)
      test_item2 = Item.new("b", 5)
      test_map.insert(test_item1)
      insert_return = test_map.insert(test_item2)
      expect(insert_return).to eq(true)
    end
  end

  context "When searching for item it should" do
    it "return nil if item doesn't exist" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      find_return = test_map.find("b")
      expect(find_return.nil?).to eq(true)
    end

    it "return item with given key" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      find_return = test_map.find("a")
      expect(find_return == test_item.value).to eq(true)
    end
  end

  context "When removing item from map it should" do
    it "return false if item with given key doesn't exist" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      remove_return = test_map.remove("b")
      expect(remove_return).to eq(false)
    end

    it "return true if item was removed" do
      test_map = Map.new
      test_item = Item.new("a", 3)
      test_map.insert(test_item)
      remove_return = test_map.remove("a")
      expect(remove_return).to eq(true)
    end
  end
end
