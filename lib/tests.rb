load 'map.rb'

# binding.pry
map = Map.new

array = [ Item.new("a", 1), Item.new("b", 2), Item.new("c", 3), Item.new("d", 4), Item.new("e", 5) ]

array.each{ |x| puts(map.insert(x)) }
# map.printTree

map.remove("c")
puts(map.depth)

# puts(map.find("c"))

# binding.pry

test_map = Map.new
test_items = [Item.new("a",1), Item.new("b",2), Item.new("c",3), Item.new("d",4), Item.new("e",5)]
test_items.each { |x| test_map.insert(x) }
puts(test_map.toString)
# puts(map.depth)
