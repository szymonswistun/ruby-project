#Szymon Świstun, mail kontaktowy: szymonswistun94@gmail.com
require 'pry'

class Item
  attr_accessor :key, :value

  def initialize(key, value)
    @key, @value = key, value
  end
end

class Node
  attr_accessor :parent, :left, :right, :data

  def initialize(data, parent = NIL)
    @data = data
    @parent = parent
  end
end

class Map
  @root = NIL
  @count = 0

  def size
    @count
  end

  def empty?
    @root.nil?
  end

  def insert(data)
    # binding.pry
    if (@root.nil?)
      @root = Node.new(data)
      unless (@count.nil?)
        @count += 1
      else
        @count = 1
      end
      true
    else
      if (@root.data.key == data.key)
        false
      else
        # binding.pry
        self.insertR(data, @root)
        unless (@count.nil?)
          @count += 1
        else
          @count = 1
        end
        true
      end
    end
  end

  def insertR(data, node)
    # binding.pry
    if (data.key == node.data.key)
      false
    elsif (data.key < node.data.key)
      if (node.left.nil?)
        node.left= Node.new(data, node)
        true
      else
        self.insertR(data, node.left)
      end
    else
      if (node.right.nil?)
        node.right= Node.new(data, node)
        true
      else
        self.insertR(data, node.right)
      end
    end
  end

  def find(key)
    ret = findR(key, @root)
    unless ret.nil?
      ret.data.value
    else
      NIL
    end

  end

  def findR(key, node)
    # binding.pry
    if (node.nil?)
      NIL
    elsif (key == node.data.key)
      node
    elsif (key < node.data.key)
      unless (node.left.nil?)
        findR(key, node.left)
      else
        NIL
      end
    else
      unless (node.right.nil?)
        findR(key, node.right)
      else
        NIL
      end
    end
  end

  def remove(key)
    # binding.pry
    if (@root.nil?)
      false
    elsif (@root.data.key == key)
      unless (@root.left.nil? && @root.right.nil?)
        if (@root.left.nil?)
          @root = @root.right
        elsif (@root.right.nil?)
          @root = @root.left
        else
          t = @root.left

          while (!t.left.nil?)
            t = t.left
          end

          unless (t.right.nil?)
            t = t.right
          end

          t.parent = NIL
          @root.data = t.data

        end
        @count -= 1
      else
        root = NIL
      end

      true
    else
      removeR(key, findR(key, @root))
    end
  end

  def removeR(key, node)
    # binding.pry
    return false if (node.nil?)
    unless (node.left.nil? && node.right.nil?)
      if (node.left.nil?)
        node.parent.right = node.right
      elsif (node.right.nil?)
        node.parent.left = node.left
      else
        t = node.left

        while (!t.left.nil?)
          t = t.left
        end

        unless (t.right.nil?)
          t = t.right
          t.parent.right = NIL
        else
          t.parent.left = NIL
        end

        node.key, node.value = t.key, t.value

      end
    else
      root = NIL
    end
  end

  def printTree
    printR(@root)
    puts
  end

  def printR(node)
    unless (node.nil?)
      printR(node.left)
      print "(#{node.data.key}, #{node.data.value}), "
      printR(node.right)
    end
  end

  def toString
    toStringR(@root)
  end

  def toStringR(node)
    unless (node.nil?)
      # toStringR(node.left)
      # print "(#{node.data.key}, #{node.data.value}), "
      # toStringR(node.right)
      "#{toStringR(node.left)}(#{node.data.key}, #{node.data.value}), #{toStringR(node.right)}"
    end
  end

  def depth
    depthR(@root)
  end

  def depthR(node)
    if (node.nil?)
      0
    else
      ret = 1
      left, right = depthR(node.left), depthR(node.right)
      if (left > right)
        ret += left
      else
        ret += right
      end
    end
  end

  def truncate
    truncateR(@root)
    @root = NIL
  end

  def truncateR(node)
    unless (node.left.nil?)
      truncateR(node.left)
      node.left = NIL
    end

    unless (node.right.nil?)
      truncateR(node.right)
      node.right = NIL
    end
  end

end
